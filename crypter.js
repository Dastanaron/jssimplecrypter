function crypt(salt, string) {
	let sumSalt = getSaltSum(salt);
	return Buffer.from(crypterCicle(string, sumSalt)).toString('base64');
}

function decrypt(salt, crypted) {
	let sumSalt = getSaltSum(salt);

	let decodedBase64 = Buffer.from(crypted, 'base64').toString();
	let decrypted = '';

	return crypterCicle(decodedBase64, sumSalt);
}

function crypterCicle(string, sumSalt) {
	let result = '';
	for (let symbol in string) {
		let char = string[symbol].charCodeAt() ^ sumSalt;
    	result += String.fromCharCode(char);
	}
	return result;
}

function getSaltSum(salt) {
	let sumSalt = 0;

	for (let symbol in salt) {
		sumSalt += salt[symbol].charCodeAt();
	}
	return sumSalt;
}

module.exports = {
	crypt: crypt,
	decrypt: decrypt,
}
