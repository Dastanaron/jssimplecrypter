const Crypter = require('./crypter.js');

const salt = 'abc';

let inputString = 'тест';

let cryptedString = Crypter.crypt(salt, inputString);

let decryptedString = Crypter.decrypt('abc', cryptedString);

console.log(cryptedString, decryptedString);
